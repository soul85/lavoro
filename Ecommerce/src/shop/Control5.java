package shop;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.OrderDao;
import database.OrderDaoImp;
import database.ProductDao;
import database.ProductDaoImp;


@WebServlet("/Control5")
public class Control5 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control5() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		int id2 = (int)(session.getAttribute("rand"));

		
		int i = Integer.parseInt(request.getParameter("idp1"));
		
		ProductDao productDao = new ProductDaoImp();
		OrderDao orderDao = new OrderDaoImp();
		
		try {
			productDao.increaseQuantity(i);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		try {
			orderDao.deleteOrder(i, id2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
//		UpdateQuantity up1= new UpdateQuantity();
//		up1.increase(i);
		
		
//		DeleteOrder del = new DeleteOrder();
//		del.delete(i, id2);


		RequestDispatcher rd4=request.getRequestDispatcher("remove.jsp");  
		rd4.forward(request, response);

	}



}
