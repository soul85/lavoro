package shop;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.*;


@WebServlet("/Control11")
public class Control11 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public Control11() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id1 = Integer.parseInt(request.getParameter("ido1"));
		List<Orders> list=null;
		
		OrderDao orderDao=new OrderDaoImp();
		
		try {
			list=orderDao.searchOrder(id1);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		


		request.setAttribute("list", list);

		RequestDispatcher rd4=request.getRequestDispatcher("details.jsp");  
		rd4.forward(request, response);
	}

	
	

}
