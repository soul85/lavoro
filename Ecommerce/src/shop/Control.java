package shop;

import java.io.IOException;


import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.*;




@WebServlet("/Control")
public class Control extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String s = request.getParameter("email");
		String st = request.getParameter("pwd");
		HttpSession session = request.getSession();

		Customer cust = new Customer(0,null,null,null,s,st);
		CustomerDao customerDao = new CustomerDaoImp();
		try {
			customerDao.login(cust);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		System.out.println(cust.getName());
		System.out.println(cust.getSurname());
		System.out.println(cust.getCountry());
		System.out.println(cust.getEmail());
		System.out.println(cust.getPassword());

		if(customerDao.validLogin()==true){
			session.setAttribute("customerId", cust.getCustomerId());
			session.setAttribute("name", cust.getName());
			session.setAttribute("surname", cust.getSurname());
			session.setAttribute("country", cust.getCountry());
			session.setAttribute("email",cust.getEmail());
			session.setAttribute("pwd", cust.getPassword());


			RequestDispatcher rd=request.getRequestDispatcher("/Control9");  
			rd.forward(request, response);  
		}
		

		else{

			RequestDispatcher rd=request.getRequestDispatcher("back.html");  
			rd.forward(request, response);
		}
	}


	

}
