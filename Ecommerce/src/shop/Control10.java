package shop;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.*;


@WebServlet("/Control10")
public class Control10 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public Control10() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		int id = (int)(session.getAttribute("customerId"));
		RecapDao recapDao = new RecapDaoImp();
		List<RecapOrder> listHistory = null;
		
		try {
			listHistory=recapDao.searchRecapCustomerId(id);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		
		request.setAttribute("history", listHistory);
		
		RequestDispatcher rd2=request.getRequestDispatcher("history.jsp");  
		rd2.forward(request, response);
		
	}

	
	

}
