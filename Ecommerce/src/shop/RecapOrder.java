package shop;

import java.util.ArrayList;




public class RecapOrder {
	private int orderNumber;
	private int orderId;
	private int customerId;
	private int priceTotal;
	String date;
	ArrayList<RecapOrder> listrecap= new ArrayList<RecapOrder>();

	public RecapOrder(int orderNumber, int orderId, int customerId,int priceTotal, String date) {

		this.orderNumber = orderNumber;
		this.orderId = orderId;
		this.customerId = customerId;
		this.priceTotal = priceTotal;
		this.date = date;
	}
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getPriceTotal() {
		return priceTotal;
	}
	public void setPriceTotal(int priceTotal) {
		this.priceTotal = priceTotal;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public ArrayList<RecapOrder> getListrecap() {
		return listrecap;
	}
	public void setListrecap(ArrayList<RecapOrder> listrecap) {
		this.listrecap = listrecap;
	}
	@Override
	public String toString() {
		return "RecapOrder [orderNumber=" + orderNumber + ", orderId="
				+ orderId + ", customerId=" + customerId + ", priceTotal="
				+ priceTotal + ", date=" + date + "]";
	}

}
