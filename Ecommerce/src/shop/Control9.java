package shop;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Control9")
public class Control9 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Control9() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		Random rand = new Random();
		int randomNum = rand.nextInt((999 - 1) + 1) + 1;
		System.out.println(randomNum);
		session.setAttribute("rand", randomNum);
		
		RequestDispatcher rd=request.getRequestDispatcher("response.jsp");  
		rd.forward(request, response);
	}

	
	

}
