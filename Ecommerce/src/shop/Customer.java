package shop;

import java.util.ArrayList;


public class Customer {
	private int customerId;
	private String name;
	private String surname;
	private String country;
	private String email;
	private String password;
	ArrayList<Customer> listCustomer =new ArrayList<Customer>();

	public Customer(int customerId, String name, String surname,String country, String email, String password) {
		this.customerId = customerId;
		this.name = name;
		this.surname = surname;
		this.country= country;
		this.email = email;
		this.password = password;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<Customer> getListCustomer() {
		return listCustomer;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name
				+ ", surname=" + surname + ", email=" + email + ", password="
				+ password + "]";
	}

}
