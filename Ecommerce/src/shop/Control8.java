package shop;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Properties;



import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.*;
import org.apache.velocity.app.VelocityEngine;


import database.*;
import shop.MailUtility;


@WebServlet("/Control8")
public class Control8 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public Control8() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		int id4 = (int)(session.getAttribute("rand"));
		int id = (int)(session.getAttribute("customerId"));
		int tot = Integer.parseInt(request.getParameter("tot"));
		String dt = request.getParameter("dat");
//		String mail = (String)session.getAttribute("email");
		String mail="paolopietro.oldrati@gmail.com";
		String name =(String)session.getAttribute("name");
		String subject = "Order "+ id4 +" from Shop Online";

		
		RecapOrder recap = new RecapOrder(0,0,0,0,null);
		recap.setOrderId(id4);
		recap.setCustomerId(id);
		recap.setPriceTotal(tot);
		recap.setDate(dt);
		RecapDao recapDao = new RecapDaoImp();
		
		try {
			recapDao.insertRecapOrder(recap);
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		Properties props = new Properties();
        props.setProperty("resource.loader", "webapp");
        props.setProperty("webapp.resource.loader.class", "org.apache.velocity.tools.view.WebappResourceLoader");
//        props.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
//        props.setProperty("class.resource.loader.path", "/Ecommerce/WebContent/WEB-INF/Templates");
		
        
		VelocityEngine ve = new VelocityEngine(props);
		ve.setApplicationAttribute("javax.servlet.ServletContext", request.getSession().getServletContext());
        ve.init();
        
        Template t = ve.getTemplate( "ordermail.vm" );

        VelocityContext context = new VelocityContext();
        context.put("name", name);
        context.put("id", id4);
        context.put("customerId",id);
        context.put("date",dt);
        context.put("total",tot);
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        String text = writer.toString();
        
        try {
			MailUtility.sendMail(mail,subject,text);
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		
		
		
		RequestDispatcher rd2=request.getRequestDispatcher("success.jsp");  
		rd2.forward(request, response);
		
	}

	
	

}
