package shop;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.*;



@WebServlet("/Control7")
public class Control7 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control7() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		int id4 = (int)(session.getAttribute("rand"));

		GregorianCalendar date =new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy � HH:mm:ss");
		String s=sdf.format(date.getTime());
		
		OrderDao orderDao = new OrderDaoImp();
		List<Orders> list=null;
		
		try {
			list=orderDao.searchOrder(id4);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		int tot =0;
		for(Orders o:list){
			int b=o.getTotalPrice();
			tot=tot+b;
		}

		request.setAttribute("list", list);
		request.setAttribute("date", s);
		request.setAttribute("total", tot);


		RequestDispatcher rd2=request.getRequestDispatcher("recap.jsp");  
		rd2.forward(request, response);


	}




}
