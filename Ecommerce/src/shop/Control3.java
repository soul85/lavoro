package shop;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.*;


@WebServlet("/Control3")
public class Control3 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control3() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String a= request.getParameter("nmp");
		int b = Integer.parseInt(request.getParameter("pr"));
		int d = Integer.parseInt(request.getParameter("idp"));
		int f = (int)(session.getAttribute("customerId"));

		int id = (int)(session.getAttribute("rand"));
		
		ProductDao productDao = new ProductDaoImp();
		OrderDao orderDao = new OrderDaoImp();
		
		try {
			productDao.decreaseQuantity(d);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

//		UpdateQuantity up= new UpdateQuantity();
//		up.decrease(d);

		Orders ord = new Orders(id,f,d,a,b);
		
		try {
			orderDao.insertOrder(ord);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
//		InsertOrder in = new InsertOrder();
//		in.run(ord);
		request.setAttribute("nm", a);
		RequestDispatcher rd2=request.getRequestDispatcher("addbasket.jsp");  
		rd2.forward(request, response);
	}


	
}
