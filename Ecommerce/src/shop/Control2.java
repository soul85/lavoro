package shop;

import java.io.IOException;
import java.sql.SQLException;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.*;

@WebServlet("/Control2")
public class Control2 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control2() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Product> listProd = null;

		
		ProductDao productDao = new ProductDaoImp();
		try {
			 listProd = productDao.searchProduct();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.setAttribute("list",listProd);

		RequestDispatcher rd2=request.getRequestDispatcher("list.jsp");  
		rd2.forward(request, response);

	}


	

}
