package shop;

import java.util.ArrayList;

public class Orders {

	private int orderId;
	private int customerId;
	private int productId;
	private int totalPrice=0;
	private String productName;
	Customer customerOrder = new Customer(0,null,null,null,null,null);
	Product productOrder = new Product(null,0,0,0,null);
	ArrayList<Orders> listOrders = new ArrayList<Orders>();



	public Orders(int orderId, int customerId, int productId, String productName,int totalPrice) {

		this.orderId = orderId;
		this.customerId = customerId;
		this.productId = productId;
		this.productName=productName;
		this.totalPrice = totalPrice;

	}



	public int getOrderId() {
		return orderId;
	}



	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}



	public int getCustomerId() {
		return customerId;
	}



	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}



	public int getProductId() {
		return productId;
	}



	public void setProductId(int productId) {
		this.productId = productId;
	}



	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}



	public int getTotalPrice() {
		return totalPrice;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public ArrayList<Orders> getListOrders() {
		return listOrders;
	}



	public void setListOrders(ArrayList<Orders> listOrders) {
		this.listOrders = listOrders;
	}



}
