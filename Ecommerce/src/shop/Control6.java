package shop;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import database.CustomerDao;
import database.CustomerDaoImp;
import shop.MailUtility;


@WebServlet("/Control6")
public class Control6 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control6() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String a = request.getParameter("name");
		String b = request.getParameter("surname");
		String c= request.getParameter("country");
		String d = request.getParameter("email");
		String e = request.getParameter("pwd");
		String subject= null;
		String text=null;


		CustomerDao customerDao = new CustomerDaoImp();	

		Customer newCustomer = new Customer(0,a,b,c,d,e);

		try {
			customerDao.insertCustomer(newCustomer);
		} catch (SQLException ex) {

			ex.printStackTrace();
		}

		Properties props = new Properties();
        props.setProperty("resource.loader", "webapp");
        props.setProperty("webapp.resource.loader.class", "org.apache.velocity.tools.view.WebappResourceLoader");

		if(c.equalsIgnoreCase("England")){


			VelocityEngine ve = new VelocityEngine(props);
			ve.setApplicationAttribute("javax.servlet.ServletContext", request.getSession().getServletContext());
			ve.init();

			Template t = ve.getTemplate( "registrationEng.vm" );

			VelocityContext context = new VelocityContext();
			context.put("name", a);
			context.put("pwd", e);
			StringWriter writer = new StringWriter();
			t.merge( context, writer );
			subject="Registration to Shop Online";
			text = writer.toString();
			
		}else if (c.equalsIgnoreCase("Italy")){

			VelocityEngine ve = new VelocityEngine(props);
			ve.setApplicationAttribute("javax.servlet.ServletContext", request.getSession().getServletContext());
			ve.init();

			Template t = ve.getTemplate( "registrationIta.vm" );

			VelocityContext context = new VelocityContext();
			context.put("name", a);
			context.put("pwd", e);
			StringWriter writer = new StringWriter();
			t.merge( context, writer );
			subject="Registrazione al Negozio Online";
			text = writer.toString();


		}


		try {
			MailUtility.sendMail("paolopietro.oldrati@gmail.com",subject,text);
		} catch (MessagingException ex) {

			ex.printStackTrace();
		}

		RequestDispatcher rd2=request.getRequestDispatcher("registered.html");  
		rd2.forward(request, response);
	}





}
