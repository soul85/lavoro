package shop;

import java.io.IOException;
import java.sql.SQLException;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.OrderDao;
import database.OrderDaoImp;


@WebServlet("/Control4")
public class Control4 extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public Control4() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		OrderDao orderDao = new OrderDaoImp();
		HttpSession session = request.getSession();
		int id2 = (int)(session.getAttribute("rand"));
		List<Orders> listOrd = null;

		try {
			listOrd=orderDao.searchOrder(id2);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//		SearchOrders sc= new SearchOrders();
		//		sc.run(id2);
		//		ArrayList<Orders> list = new ArrayList<Orders>();
		//
		//		list=sc.getOrder1().getListOrders();


		request.setAttribute("list", listOrd);

		RequestDispatcher rd4=request.getRequestDispatcher("basket.jsp");  
		rd4.forward(request, response);
	}




}
