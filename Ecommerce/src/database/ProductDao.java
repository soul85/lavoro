package database;

import java.sql.SQLException;
import java.util.List;
import shop.Product;

public interface ProductDao {
	public List<Product> searchProduct()throws SQLException;
	public void  increaseQuantity(int i)throws SQLException;
	public void decreaseQuantity(int i) throws SQLException;
}
