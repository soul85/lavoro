package database;

import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConnectionDb {
	
//	private static final String USERNAME = "root";
//
//	private static final String PASSWORD = "paolo";
//
//	private static final String SERVERNAME = "localhost";
//
//	private static final int PORTNUMBER = 3306;
//
//	private static final String DBNAME = "shop";
//	
//	private static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
//	
//	private static ConnectionDb instance;
//	
//	private ConnectionDb() {
//		try {
//			Class.forName(DRIVER_CLASS);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	private Connection createConnection() {
//		Connection conn = null;
//		try {
//		
//			conn = DriverManager.getConnection("jdbc:mysql://"
//					+ SERVERNAME + ":" + PORTNUMBER + "/" + DBNAME,
//					USERNAME,PASSWORD);
//			
//		} catch (SQLException e) {
//			System.out.println("ERROR: Unable to Connect to Database.");
//			e.printStackTrace();
//		}
//		return conn;
//	}	
//	
//	public static Connection getConnection() throws SQLException {
//			if (instance == null){
//				instance= new ConnectionDb();
//			}
//			return instance.createConnection();
//	}
	@Resource(lookup="java:comp/env/jdbc/shop")
	public static Connection getConnection() throws SQLException {
		Connection conn = null;
		Context context=null;
		DataSource datasource=null;
		try {
			context = new InitialContext();
			datasource = (DataSource) context.lookup("java:/comp/env/jdbc/shop");
			conn = datasource.getConnection();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return conn;
	}
}
