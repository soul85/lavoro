package database;

import java.sql.SQLException;

import shop.Customer;

public interface CustomerDao {
		public void insertCustomer(Customer o)throws SQLException;
		public Customer login(Customer o)throws SQLException;
		public boolean validLogin();
}
