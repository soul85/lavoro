package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import shop.Customer;
import database.ConnectionDb;

public class CustomerDaoImp implements CustomerDao {
	
	private static final String TABLENAME="customers";
	
	private boolean valid=true;
	
	public Boolean executeQuery(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	public boolean executeUpdate(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	
	@Override
	public void insertCustomer(Customer o) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "insert into "+ TABLENAME+ " values (null,?,?,?,?,?)";
			ps=conn.prepareStatement(createString);
			ps.setString(1, o.getName());
			ps.setString(2, o.getSurname());
			ps.setString(3, o.getCountry());
			ps.setString(4, o.getEmail());
			ps.setString(5,o.getPassword());
			ps.executeUpdate(); 

			System.out.println("insert ok");
			
		} catch (SQLException e) {
			System.out.println("insert error");
			e.printStackTrace();
			return;
			
		}
		
	}

	@Override
	public Customer login(Customer o)throws SQLException {
		PreparedStatement ps = null;
		
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "SELECT * FROM " + TABLENAME + " where email= ? and password=?";
			
			ps=conn.prepareStatement(createString);
			ps.setString(1, o.getEmail());
			ps.setString(2, o.getPassword());
			
			ResultSet res =ps.executeQuery(); 


			if (res.next()== true) {

				int a = Integer.parseInt(res.getString("customerId"));
				String b = res.getString("name");
				String c=res.getString("surname");
				String d= res.getString("country");
				String e=res.getString("email");
				String f=res.getString("password");
				
				o.setCustomerId(a);
				o.setName(b);
				o.setSurname(c);
				o.setCountry(d);
				o.setEmail(e);
				o.setPassword(f);
			
			}else{
				valid=false;
				System.out.println("login error ");
			}
			res.close();			
		}catch (SQLException e) {
				System.out.println("ERROR");
				e.printStackTrace();
				
			}
		return o;
	}
	@Override
	public boolean validLogin() {
		
		return valid;
	}

}
