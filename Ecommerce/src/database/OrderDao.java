package database;

import java.sql.SQLException;
import java.util.List;

import shop.Orders;

public interface OrderDao {
		public void insertOrder(Orders o)throws SQLException;
		public void deleteOrder(int m,int n) throws SQLException;
		public List<Orders> searchOrder(int l)throws SQLException;
		
}
