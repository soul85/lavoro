package database;

import java.sql.SQLException;
import java.util.List;

import shop.*;

public interface RecapDao {
	public void insertRecapOrder(RecapOrder o)throws SQLException;
	public List<RecapOrder> searchRecapOrder(int i)throws SQLException;
	public List<RecapOrder> searchRecapCustomerId(int i)throws SQLException;
	
}
