<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<title>Register</title>
<link href="footer.css" rel="stylesheet">



</head>
<body>
	<div id="wrap">
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
		
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="welcome.html">Logout <span class="glyphicon glyphicon-off" ></span></a></li>

				</ul>
			</div>
		</nav>
		     
		
		
		<div class="container">
			<div class="row">

				<div class="col-md-6 col-md-offset-3"
					style="background-color: #CADABA;">
					<br>
					<h2>insert your information</h2>


					<br>
					<form  name="value" action="/Ecommerce/Control6" method="get"
						class="col-md-12" >

						<div class="form-group ">
							<input type="text" class="form-control input-lg"
								placeholder="Name" name="name"
								required >
								
						</div>
						<br>

						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="Surname" name="surname"
								required >
						</div>
						<br>

						<div class="form-group">
						
							<select class="form-control input-lg" name="country" required>
								<option value="">Please select a country
  								<option>Italy
  								<option>England
							    
							</select>
						</div>
								
							
						
						<br>
						<div class="form-group">
							<input type="email" required placeholder="Enter a valid email address" class="form-control input-lg"
								placeholder="Email" name="email">
						</div>
						<br>
						
						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="Password" name="pwd" required >
						</div>
						<br> <input type="submit" class="btn btn-primary"
							value="enter" />
					</form>
					
				</div>
			</div>
		</div>

	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>
</html>