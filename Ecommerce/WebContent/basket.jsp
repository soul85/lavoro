<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="shop.*,java.util.*"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<title>Basket</title>
<link href="footer.css" rel="stylesheet">
</head>
<body>
	<div id="wrap">
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/Ecommerce/Control2">Shop</a></li>
					<li><a href="welcome.html">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>

				</ul>
				<p class="navbar-text"><%=session.getAttribute("name")%>
					<%= session.getAttribute("surname")%> <span class="glyphicon glyphicon-user" ></span></p>
			</div>
		</nav>

		<%! @SuppressWarnings("unchecked") %>
		<% ArrayList<Orders> list = (ArrayList<Orders>)request.getAttribute("list"); 
	int tot=0;
	%>
	<div class="container" >
			<div class="row">

				<div class="col-md-12 " style ="background-color: #E4E5E0;" >
						<h2>Basket</h2>
						<br>
						<table class="table">
							<thead>
								<tr>
									<th>Product</th>
									<th>Price</th>
									<th>Order Id</th>
								</tr>
							</thead>
							<% for (Orders o:list){
						int a=o.getProductId();
						String c = o.getProductName();
						int b=o.getTotalPrice();
						tot=tot+b;%>
							<tbody>
								<tr>
									<td><%= c %></td>
									<td><%= b %></td>
									<td><%=session.getAttribute("rand")%></td>

									<td>
										<form name="nome" action="/Ecommerce/Control5" method="get">
											<input type="Hidden" name="idp1" value="<%= a %>"> 
												<input type="submit" class="btn btn-success " value="remove" /> <br>
										</form>
									</td>
								</tr>
							</tbody>

							<% } %>
						</table>
					</div>
				</div>
			</div>
	<div class="container" >
			<div class="row">

				<div class="col-md-3 col-md-offset-10" >
						<p class="lead">
							total price:<%=tot %> euro
												
						</p>
						<div class="col-md-3 col-md-offset-5">
						<form name="pay" action="/Ecommerce/Control7" method="get">
							<button type="submit" class="btn btn-primary btn-lg" >Pay</button>
						</form>
						</div>
						
					</div>
				</div>
			</div>

	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>
</html>