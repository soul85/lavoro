<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="shop.*,java.util.*"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<title>Recap</title>
<link href="footer.css" rel="stylesheet">
</head>
<body>
	<div id="wrap">
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/Ecommerce/Control2">Shop</a></li>
					<li><a href="/Ecommerce/Control4">Basket <span class="glyphicon glyphicon-shopping-cart" ></span> </a></li>
					<li><a href="welcome.html">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>

				</ul>
				<p class="navbar-text"><%=session.getAttribute("name")%>
					<%= session.getAttribute("surname")%> <span class="glyphicon glyphicon-user" ></span> </p>
			</div>
		</nav>
		<br>
		<br>
	<div style ="background-color: #E4E5E0;">
			<h2>Order details</h2>
			<br>
			<%! @SuppressWarnings("unchecked") %>
			<% ArrayList<Orders> list = (ArrayList<Orders>)request.getAttribute("list");
			%>
			<table class="table">
				<thead>
					<tr>
						
						<td>Order Id</td>
						<td>Customer Id</td>
						<td>Date</td>
						<td>Total Price</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						
						<td><%=session.getAttribute("rand")%></td>
						<td><%=session.getAttribute("customerId") %></td>
						<td><%=request.getAttribute("date") %></td>
						<td><%=request.getAttribute("total") %></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-12">
					<form name="pay2" action="/Ecommerce/Control8" method="get">
					<input type="Hidden" name="tot" value="<%=request.getAttribute("total")%>">
					<input type="Hidden" name="dat" value="<%=request.getAttribute("date")%>">
						<button type="submit" class="btn btn-primary btn-lg">Pay</button>
					</form>
				</div>
			</div>
		</div>



	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>
</html>