<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="shop.*,java.util.*"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<title>Success</title>
<link href="footer.css" rel="stylesheet">
</head>
<body>
	<div id="wrap">
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/Ecommerce/Control9">Shop</a></li>
					
					<li><a href="welcome.html">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>

				</ul>
				<p class="navbar-text"><%=session.getAttribute("name")%>
					<%= session.getAttribute("surname")%> <span class="glyphicon glyphicon-user" ></span> </p>
			</div>
		</nav>
		<br>
		<br>
		<p class="lead">Your orders has been shipped</p>

		<a href="/Ecommerce/Control9"> go to shop </a>
		



	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>
</html>