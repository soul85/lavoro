<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="shop.*,java.util.*"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<title>List</title>
	<style type="text/css">
		.img-rounded {
 	 		border-radius: 8px;
		}
		
	</style>
	<link href="footer.css" rel="stylesheet">
</head>
<body>
	<div id="wrap">

		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/Ecommerce/Control4">Basket <span class="glyphicon glyphicon-shopping-cart" ></span> </a></li>
					<li><a href="welcome.html">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>
					<li><a href="/Ecommerce/Control10">Order History </a></li>
				</ul>
				<p class="navbar-text"><%=session.getAttribute("name")%>
					<%=session.getAttribute("surname")%> <span class="glyphicon glyphicon-user" ></span> </p>
			</div>
		</nav>

		<%!@SuppressWarnings("unchecked")%>
		<%
			ArrayList<Product> list = (ArrayList<Product>) request
					.getAttribute("list");
		%>
		
		<div class="container" >
			<div class="row">
				<div class ="col-md-4">
					<h2>List of Products</h2>
				</div>
			</div>
		</div>
		
		<br>

		<%
			for (Product p : list) {
				int id = p.getProductId();
				String nm = p.getName();
				int l = p.getPrice();
				int v = p.getQuantity();
				String im = p.getImageLink();
				boolean b = p.outOfStock();
				Product pd = new Product(nm, l, id, v,im);

				if (b == false) {
		%>
		
		<div class="container" style ="background-color: #E4E5E0;">
			<div class="row">
				<div class="col-md-4">
					<h2>
						<%=nm%>
					</h2>
					<img alt="<%=nm%>" src="<%= im %>" width=auto height="400" class="img-rounded" >
					price:
					<%=l%>
					euro
				</div>
			</div>
			
			<form name="nome" action="/Ecommerce/Control3" method="get">

				<input type="Hidden" name="nmp" value="<%=nm%>"> 
				<input type="Hidden" name="pr" value="<%=l%>">
				<input type="Hidden" name="quantity" value="<%=v%>"> 
				<input type="Hidden" name="idp" value="<%=id%>">
				<input type="hidden" name="product" value="<%=pd%>"> 
				<input type="submit" class="btn btn-primary" value="add to basket" >
				<span class="glyphicon glyphicon-shopping-cart" ></span><br>
				<p class="text-success"> product in stock: 
				<%=v%>
				Pz available </p>
			</form>
		</div>
		<br>
		<br>

		<%
			} else {
		%>
		<div class="container" style ="background-color: #E4E5E0;">
			<div class="row">
				<div class="col-md-4">
					<h2>
						<%=nm%>
					</h2>
					<img alt="<%=nm%>" src="<%= im %>" width=auto height="400" class="img-rounded">
					price:
					<%=l%>
					euro
				</div>
			</div>
			<form name="nome" action="outofstock.html" method="get">

				<input type="Hidden" name="nmp" value="<%=nm%>"> 
				<input type="Hidden" name="pr" value="<%=l%>"> 
				<input type="Hidden" name="quantity" value="<%=v%>"> 
				<input type="Hidden" name="idp" value="<%=id%>">
				<input type="hidden" name="product" value="<%=pd%>"> 
				<input type="submit" class="btn btn-primary" value="add to basket" > 
				<span class="glyphicon glyphicon-shopping-cart" ></span> <br>
				<p class="text-danger"> product out of stock </p>
			</form>

		</div>
		<br>
		<br>
		<%
			}

			}
		%>

	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>



</html>